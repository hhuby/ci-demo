package ws.synopsis.cidemo.service;

import java.util.List;
import java.util.Map;

import ws.synopsis.cidemo.domain.Demo;

public interface DemoService {

	Map<String, Object> save();

	List<Demo> findAll();

}
