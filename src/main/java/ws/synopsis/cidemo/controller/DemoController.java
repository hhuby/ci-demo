package ws.synopsis.cidemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/demo")
public class DemoController {

	@GetMapping("/")
	public String findAll() {
		String text = "Hola mundo";

		if (true) {
			text = "";
		}

		return "index";
	}

	@GetMapping("/{id}")
	public String findAllById() {
		String text = "Hola mundo";

		if (true) {
			text = "";
		}

		return "index";
	}

	@PostMapping("/")
	public String save() {
		String text = "Hola mundo";

		if (true) {
			text = "";
		}

		return "index";
	}

	@PutMapping("/")
	private String update() {
		String text = "Hola mundo";
		if (true) {
			text = "";
		}

		return text;
	}

	@PutMapping("/")
	private String delete() {
		String text = "Hola mundo";
		if (true) {
			text = "";
		}

		return text;
	}

}
